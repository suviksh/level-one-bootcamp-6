//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>
#include<math.h>

int NumberOfEntries(int n)
{
    printf("No. of entries: ");
    scanf("%d",&n);
    while(n<=0)
    {
        printf("Enter a positive number of entries: ");
        scanf("%d",&n);
    }
    return n;
}

int Entries(int n, int arr[])
{
    int i = 0;
    for (i = 0; i<n; i++)
    {
        printf("Element %d: ",i);
        scanf("%d",&arr[i]);
    }
}

int SumOfN(int n, int arr[])
{
    int sum=0;
    for (int i=0; i<n; i++)
    {
        sum += arr[i];
    }
    return sum;
}

int output(int n, int arr[])
{
    printf("%d",SumOfN(n,arr));
}

int main()
{
    int n = NumberOfEntries(n);
    int arr[n];
    Entries(n, arr);
    SumOfN(n, arr);
    printf("Sum of these %d numbers:\n",n);
    output(n,arr);
    return 0;
}
